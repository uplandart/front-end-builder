module.exports = {
	rmdir: [ "public" ],

	angularjs: {
		publicCore: {
			templates: {
				src: [ "src/angular/**/*.pug" ],
				dest: "public/angular",
				concat: true,
				args: {
					filename: "core-templates.js",
					module: "app.core"
				}
			},
			build: {
				src: [ "src/angular/**/*.js" ],
				dest: "public/angular",
				map: true,
				iife: true,
				concat: {
					name: "app.core.bundle",
					iife: true
				}
			}
		},
	},

	js: {
		public: {
			build: {
				src: "src/js/*.js",
				dest: "public/js"
			}
		},
		publicLibs: {
			build: {
				src: "src/js/libs/*.js",
				dest: "public/js/libs"
			}
		}
	},

	pug: {
		public: {
			build: {
				src: [ 'src/views/**/*.pug', '!src/views/hide/**/*.pug' ],
				dest: 'public'
			},
			watch: {
				src: 'src/views/**/*.pug'
			}
		},
		angularjs: {
			build: {
				src: 'src/angular/**/*.pug',
				dest: 'public/angular'
			}
		}
	},

	stylus: {
		public: {
			build: {
				src: 'src/css/common.styl',
				dest: 'public/css',
			},
			watch: {
				src: 'src/css/**/*.*'
			}
		},
		angularjs: {
			build: {
				src: 'src/angular/**/*.styl',
				dest: 'public/css',
				concat: 'angular.bundle'
			}
		}
	},

	components: {
		js: {
			angular: {
				src: [
					"src/bower_components/angular/angular.min",
					"src/bower_components/angular-animate/angular-animate.min",
					"src/bower_components/angular-aria/angular-aria.min",
					"src/bower_components/angular-messages/angular-messages.min",
					"src/bower_components/angular-material/angular-material.min",
					"src/bower_components/angular-ui-router/release/angular-ui-router.min",
					"src/bower_components/angular-socket-io/socket.min",
				],
				dest: "public/js/libs",
				concat: "angular.core"
			},
			main: {
				src: [
					"node_modules/moment/min/moment.min",
				],
				dest: "public/js/libs",
			}
		},
		css: {
			main: {
				src: [
					"src/bower_components/angular-material/angular-material.min",
					"src/bower_components/angular/angular-csp"
				],
				dest: "public/css/libs"
			},
			icomoon: {
				src: "src/bower_components/icomoon-bower/style",
				dest: "public/css/libs/icomoon"
			}
		},
		images: {
			main: {
				src: "src/images/**/*",
				dest: "public/images"
			}
		},
		fonts: {
			"material-design-iconsets": {
				src: [ "src/bower_components/material-design-iconsets/iconsets/**/*" ],
				dest: "public/fonts/icons/material-design"
			},
			"icomoon-icons": {
				src: [ "src/bower_components/icomoon-bower/fonts/*" ],
				dest: "public/css/libs/icomoon/fonts"
			}
		},
		other: {
			favicon: {
				src: "src/other/favicon.ico",
				dest: "public"
			}
		}
	},

	rename: {
		js: {
			"public/js/libs": [
				{ src: "node_modules/moment/min/locales.min", name: "moment.locales.min" }
			]
		}
	}
};