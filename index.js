'use strict';

const fs = require('fs');
const path = require('path');

const gulp = require('gulp');

const PATH_TO_ROOT_DIR = path.join(__dirname,'..','..');
// const PATH_TO_ROOT_DIR = path.join(__dirname,'..','test-front-end-builder');
const PATH_TO_CONFIG_FILE = path.join(PATH_TO_ROOT_DIR,'front-end-builder.config.js');

let config = require(PATH_TO_CONFIG_FILE);
// if (!config.cwd) throw Error('Config file must contains CWD');
let cwd = PATH_TO_ROOT_DIR; //config.cwd;

const argv = process.argv;
let devmode = 0;
let watch = 0;

if (argv.length > 2) {
	if (argv[2] === 'development') {
		devmode = 1;
	}
	if (argv.length > 3) watch = 1;
}

// TASKS
const build_tasks = [];
const watch_tasks = [];

let lib_dir = fs.readdirSync(path.join(__dirname,'lib'));
lib_dir = lib_dir.map(file => file.replace(/\.js/g,''));
// console.log('lib_dir', lib_dir);
for (let key in config) {
	if (!config.hasOwnProperty(key)) continue;
	if (lib_dir.includes(key)) {
		require(path.join(__dirname,'lib',key))(gulp, { cwd, devmode, watch, build_tasks, watch_tasks, getWatch }, config[key]);
	}
}

gulp.task('build', build_tasks, () => {
	if (!watch) console.log('Build done success');
});
gulp.task('watch', watch_tasks, () => {});

gulp.task('default', ['build','watch']);
gulp.task('init', ['build']);

// WATCH
function watchWatcher(watcher, title, fn) {
	watcher.on('change', (event) => {
		console.log(`${title} "${event.type}" in path: ${event.path}`);
		if (fn) fn(event);
	});
}

function getWatch(opts) {
	return () => {
		let watcher = gulp.watch(opts.src, { cwd }, opts.tasks);
		watchWatcher(watcher, opts.title, opts.fn);
	}
}

// FOR npm run
console.log("MODE", devmode ? 'development' : 'production');
if (watch) console.log('with watch changes');

if (watch) {
	console.log('Start watch src directory');
	gulp.start('default');
} else {
	console.log('Please, wait for compile project');
	gulp.start('init');
}