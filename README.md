# Front-end builder

Not for all! (I use it for my projects)

Create `front-end-builder.config.js` file in root directory;

This builder use only:
* rmdir (delete folder recursive);
* pug -> html (pretty, change extname);
* pug -> js (concat, uglify);
* stylus -> css (sourcemap, autoprefixer, concat, clean);
* js (sourcemap, babel, iife, concat, uglify);
* angularjs (templates, sourcemap, babel, iife, concat, uglify);
* components
  + js (iife, concat, uglify);
  + css (concat, clean);
  + images (minify);
  + fonts;
  + other;
* rename (soon)
  + js (uglify);
  + css (clean);
  + other;

## package.json scripts:

insert into "scripts":

```
...
"build": "node ./node_modules/front-end-builder production",
"builder": "node ./node_modules/front-end-builder production listen",
"build-dev": "node ./node_modules/front-end-builder development",
"builder-dev": "node ./node_modules/front-end-builder development listen"
...
```

## use

* build (production)
```
npm run build
```
* build and start listening changes (production)
```
npm run builder
```
* build (development)
```
npm run build-dev
```
* build and start listening changes (development)
```
npm run builder-dev
```

## front-end-builder.config.js

example in module example directory