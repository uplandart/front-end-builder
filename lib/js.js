'use strict';

const gIf = require('gulp-if');
const gConcat = require('gulp-concat');
const gSourcemaps = require('gulp-sourcemaps');
const gBabel = require('gulp-babel');
const gUglify = require('gulp-uglify');
const gInsert = require('gulp-insert');
const gWrap = require('gulp-wrap');

module.exports = (gulp, gulp_args, jss) => {

	for (let key in jss) {
		if (!jss.hasOwnProperty(key)) continue;
		let js = jss[key];

		gulp_args.build_tasks.push(`compile_js_${key}`);
		gulp_args.watch_tasks.push(`watch_compile_js_${key}`);
		gulp.task(`compile_js_${key}`, compileJS(js.build));
		gulp.task(`watch_compile_js_${key}`, gulp_args.getWatch({
			src: js.watch && js.watch.src || js.build.src,
			tasks: [`compile_js_${key}`],
			title: `Watch js ${key}`
		}));
	}

	function compileJS(opts) {
		return () => {
			return gulp.src(opts.src, { cwd: gulp_args.cwd })
					.pipe(gIf(!!opts.map, gSourcemaps.init(typeof opts.map === 'object' ? opts.map : { loadMaps: true })) )
					.pipe(gBabel({
						presets: ['babel-preset-es2015','babel-preset-es2016'].map(require.resolve)
					}))
					// .pipe(gDebug())
					.pipe(gIf(opts.iife, gWrap('(function(){\n<%= contents %>\n})();')))
					.pipe(gIf(!!opts.concat, gConcat(`${getConcatName(opts.concat)}.js`)))
					.pipe(
							gIf(
									opts.concat && opts.concat.iife,
									gInsert.transform((contents, file) => {
										contents = contents.replace(/["']use strict["'];\n?/g,'');
										let prepend = opts.concat.iife.prepend || [];
										if (!Array.isArray(prepend)) prepend = [prepend];
										contents = `(function(${prepend.join(',')}){\n"use strict";\n` + contents;
										let append = opts.concat.iife.append || [];
										if (!Array.isArray(append)) append = [append];
										contents += `\n})(${append.join(',')});`;
										return contents;
									})
							)
					)
					.pipe(gIf(!gulp_args.devmode, gUglify()))
					.pipe(gIf(!!opts.map, gSourcemaps.write('./')))
					// .pipe(gDebug())
					.pipe(gulp.dest(opts.dest, { cwd: gulp_args.cwd }));
		};
	}
};

function getConcatName(concat) {
	if (!concat) return;
	if (typeof concat === 'string') {
		return concat;
	} else {
		return concat.name;
	}
}