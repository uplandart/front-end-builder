'use strict';

const path = require('path');
const fs = require('fs');

module.exports = (gulp, gulp_args, rmdir) => {
	for (let i = 0; i < rmdir.length; i++) {
		deleteFolderRecursive(path.join(gulp_args.cwd, rmdir[i]));
	}
	console.log('All previous files success removed');
};

function deleteFolderRecursive(path) {
	if( fs.existsSync(path) ) {
		fs.readdirSync(path).forEach(file => {
			let curPath = path + "/" + file;
			if(fs.lstatSync(curPath).isDirectory()) { // recurse
				deleteFolderRecursive(curPath);
			} else { // delete file
				fs.unlinkSync(curPath);
			}
		});
		fs.rmdirSync(path);
	}
}