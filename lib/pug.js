'use strict';

const gIf = require('gulp-if');
const gPug = require('gulp-pug');
const gRename = require('gulp-rename');

module.exports = (gulp, gulp_args, pugs) => {

	for (let key in pugs) {
		if (!pugs.hasOwnProperty(key)) continue;
		let pug = pugs[key];

		gulp_args.build_tasks.push(`compile_pug_${key}`);
		gulp_args.watch_tasks.push(`watch_compile_pug_${key}`);
		gulp.task(`compile_pug_${key}`, compilePug(pug.build));
		gulp.task(`watch_compile_pug_${key}`, gulp_args.getWatch({
			src: pug.watch && pug.watch.src || pug.build.src,
			tasks: [ `compile_pug_${key}` ],
			title: `Watch pug ${key}`
		}));
	}

	function compilePug(opts) {
		return () => {
			return gulp.src(opts.src, { cwd: gulp_args.cwd })
					.pipe(gPug({
						pretty: gulp_args.devmode,
					}))
					.pipe(gIf(!!opts.extname, gRename(path => {
						// console.log('gRename', JSON.stringify(path, null, 4));
						path.extname = `.${opts.extname}`;
						return path;
					})))
					.pipe(gulp.dest(opts.dest, { cwd: gulp_args.cwd }));
		}
	}
};