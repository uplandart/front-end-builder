'use strict';

const path = require('path');

const gPug = require('gulp-pug');
const gIf = require('gulp-if');
const gForeach = require('gulp-foreach');
const gConcat = require('gulp-concat');
const gUglify = require('gulp-uglify');

module.exports = (gulp, gulp_args, pugs) => {

	for (let key in pugs) {
		if (!pugs.hasOwnProperty(key)) continue;
		let pug = pugs[key];

		gulp_args.build_tasks.push(`compile_pug_js_${key}`);
		gulp_args.watch_tasks.push(`watch_compile_pug_js_${key}`);
		gulp.task(`compile_pug_js_${key}`, compilePugToJS(pug.build));
		gulp.task(`watch_compile_pug_js_${key}`, gulp_args.getWatch({
			src: pug.watch && pug.watch.src || pug.build.src,
			tasks: [ `compile_pug_js_${key}` ],
			title: `Watch pug to js ${key}`
		}));
	}

	function compilePugToJS(opts) {
		return () => {
			return gulp.src(opts.src, { cwd: gulp_args.cwd })
			// .pipe(gDebug())
			// 		.pipe(gNewer('./public/js/templates/all.js'))
			// .pipe(gDebug())
					.pipe(gForeach( (stream, file) => {
						let filename = path.basename(file.path, '.pug');
						return stream
								.pipe(gPug({
									client: true,
									name: filename
								}));
					}))
					.pipe(gIf(opts.concat, gConcat(`${opts.concat}.js`)))
					.pipe(gIf(!gulp_args.devmode, gUglify()))
					.pipe(gulp.dest(opts.dest, { cwd: gulp_args.cwd }))
		}
	}
};