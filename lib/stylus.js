'use strict';

const gIf = require('gulp-if');
const gSourcemaps = require('gulp-sourcemaps');
const gConcat = require('gulp-concat');
const gCleanCss = require('gulp-clean-css');
const gStylus = require('gulp-stylus');
const gAutoprefixer = require('gulp-autoprefixer');

module.exports = (gulp, gulp_args, csss) => {

	for (let key in csss) {
		if (!csss.hasOwnProperty(key)) continue;
		let css = csss[key];

		gulp_args.build_tasks.push(`compile_css_${key}`);
		gulp_args.watch_tasks.push(`watch_compile_css_${key}`);
		gulp.task(`compile_css_${key}`, compileCSS(css.build));
		gulp.task(`watch_compile_css_${key}`, gulp_args.getWatch({
			src: css.watch && css.watch.src || css.build.src,
			tasks: [ `compile_css_${key}` ],
			title: `Watch css ${key}`
		}));
	}

	function compileCSS(opts) {
		return () => {
			return gulp.src(opts.src, { cwd: gulp_args.cwd })
					.pipe(gIf(!!opts.map, gSourcemaps.init(typeof opts.map === 'object' ? opts.map : { loadMaps: true })) )
					.pipe(gStylus())
					.pipe(gAutoprefixer())
					// .pipe(gDebug())
					.pipe(gIf(!!opts.concat, gConcat(`${opts.concat}.css`)))
					// .pipe(gDebug())
					.pipe(gIf(!gulp_args.devmode, gCleanCss()))
					.pipe(gIf(!!opts.map, gSourcemaps.write('./')))
					.pipe(gulp.dest(opts.dest, { cwd: gulp_args.cwd }));
		}
	}
};