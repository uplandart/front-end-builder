'use strict';

const path = require('path');
const browserify = require('browserify');
const tsify = require('tsify');
const vinylSourceStream = require('vinyl-source-stream');
const vinylBuffer = require('vinyl-buffer');

const gIf = require('gulp-if');
const gUglify = require('gulp-uglify');
const gSourcemaps = require('gulp-sourcemaps');

module.exports = (gulp, gulp_args, angulars) => {

	for (let key in angulars) {
		if (!angulars.hasOwnProperty(key)) continue;
		let angular = angulars[key];

		gulp_args.build_tasks.push(`compile_angular_2_${key}`);
		gulp_args.watch_tasks.push(`watch_compile_angular_2_${key}`);
		gulp.task(`compile_angular_2_${key}`, compileTS({
			src: angular.build.src,
			dest: angular.build.dest,
			concat: angular.build.concat || key
		}));
		gulp.task(`watch_compile_angular_2_${key}`, gulp_args.getWatch({
			src: angular.watch && angular.watch.src || angular.build.src,
			tasks: [ `compile_angular_2_${key}` ],
			title: `Watch angular 2 ${key}`
		}));
	}

	function compileTS(opts) {
		return () => {
			return browserify({
				debug: gulp_args.devmode,
				entries: opts.src,
			})
					.plugin(tsify, require(opts.tsconfig || path.join(__dirname,'..','tsconfig.json')))
					.bundle()
					.pipe(vinylSourceStream(`${opts.concat}.js`))
					.pipe(vinylBuffer())
					.pipe(gSourcemaps.init({loadMaps: true}))
					// .pipe(uglify())
					.pipe(gIf(!gulp_args.devmode, gUglify()))
					.pipe(gSourcemaps.write('./'))
					// .pipe(gIf(!gulp_args.devmode, gUglify()))
					.pipe(gulp.dest(opts.dest, { cwd: gulp_args.cwd }));
		}
	}
};