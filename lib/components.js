'use strict';

const gIf = require('gulp-if');
const gConcat = require('gulp-concat');
const gUglify = require('gulp-uglify');
const gCleanCss = require('gulp-clean-css');
const gImage = require('gulp-image');
const gInsert = require('gulp-insert');
const gWrap = require('gulp-wrap');

module.exports = (gulp, gulp_args, components) => {

	if (components.js) {
		let components_js = components.js;
		for (let key in components_js) {
			if (!components_js.hasOwnProperty(key)) continue;
			gulp_args.build_tasks.push(`compile_components_js_${key}`);
			gulp.task(`compile_components_js_${key}`, compileComponentsJS(components_js[key]));
		}
	}
	if (components.css) {
		let components_css = components.css;
		for (let key in components_css) {
			if (!components_css.hasOwnProperty(key)) continue;
			gulp_args.build_tasks.push(`compile_components_css_${key}`);
			gulp.task(`compile_components_css_${key}`, compileComponentsCSS(components_css[key]));
		}
	}
	if (components.images) {
		let components_images = components.images;
		for (let key in components_images) {
			if (!components_images.hasOwnProperty(key)) continue;
			gulp_args.build_tasks.push(`compile_components_images_${key}`);
			gulp.task(`compile_components_images_${key}`, compileComponentsImages(components_images[key]));
		}
	}
	if (components.fonts) {
		let components_fonts = components.fonts;
		for (let key in components_fonts) {
			if (!components_fonts.hasOwnProperty(key)) continue;
			gulp_args.build_tasks.push(`compile_components_fonts_${key}`);
			gulp.task(`compile_components_fonts_${key}`, compileComponentsFonts(components_fonts[key]));
		}
	}
	if (components.other) {
		let components_other = components.other;
		for (let key in components_other) {
			if (!components_other.hasOwnProperty(key)) continue;
			gulp_args.build_tasks.push(`compile_components_other_${key}`);
			gulp.task(`compile_components_other_${key}`, compileComponentsOther(components_other[key]));
		}
	}

	function compileComponentsJS(opts) {
		return () => {
			let src = opts.src;
			if (!Array.isArray(src)) src = [src];
			src = src.map(path => {
				if (!/\.js$/.test(path)) path += `.js`;
				return path;
			});
			return gulp.src(src, { cwd: gulp_args.cwd })
					.pipe(gIf(opts.iife, gWrap('(function(){\n"use strict";\n<%= contents %>\n})();')))
					.pipe(gIf(!!opts.concat, gConcat(`${getConcatName(opts.concat)}.js`)))
					.pipe(
							gIf(
									opts.concat && opts.concat.iife,
									gInsert.transform((contents, file) => {
										contents = contents.replace(/["']use strict["'];\n?/g,'');
										let prepend = opts.concat.iife.prepend || [];
										if (!Array.isArray(prepend)) prepend = [prepend];
										contents = `(function(${prepend.join(',')}){\n"use strict";\n` + contents;
										let append = opts.concat.iife.append || [];
										if (!Array.isArray(append)) append = [append];
										contents += `\n})(${append.join(',')});`;
										return contents;
									})
							)
					)
					.pipe(gIf(!gulp_args.devmode, gUglify()))
					.pipe(gulp.dest(opts.dest, { cwd: gulp_args.cwd }));
		}
	}
	function compileComponentsCSS(opts) {
		return () => {
			let src = opts.src;
			if (!Array.isArray(src)) src = [src];
			src = src.map(path => {
				if (!/\.css$/.test(path)) path += `.css`;
				return path;
			});
			return gulp.src(src, { cwd: gulp_args.cwd })
					.pipe(gIf(!!opts.concat, gConcat(`${opts.concat}.css`)))
					.pipe(gIf(!gulp_args.devmode, gCleanCss()))
					.pipe(gulp.dest(opts.dest, { cwd: gulp_args.cwd }));
		}
	}
	function compileComponentsFonts(opts) {
		return () => {
			let src = opts.src;
			if (!Array.isArray(src)) src = [src];
			src = src.map(path => {
				path += `.{eot,svg,ttf,woff,woff2,otf}`;
				return path;
			});
			return gulp.src(src, { cwd: gulp_args.cwd })
					.pipe(gulp.dest(opts.dest, { cwd: gulp_args.cwd }));
		}
	}
	function compileComponentsImages(opts) {
		return () => {
			return gulp.src(opts.src, { cwd: gulp_args.cwd })
					.pipe(gIf(!gulp_args.devmode, gImage(opts.args || {})))
					.pipe(gulp.dest(opts.dest, { cwd: gulp_args.cwd }));
		}
	}
	function compileComponentsOther(opts) {
		return () => {
			return gulp.src(opts.src, { cwd: gulp_args.cwd })
					.pipe(gulp.dest(opts.dest, { cwd: gulp_args.cwd }));
		}
	}

	function getConcatName(concat) {
		if (!concat) return;
		if (typeof concat === 'string') {
			return concat;
		} else {
			return concat.name;
		}
	}
};