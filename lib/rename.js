'use strict';

const gRename = require("gulp-rename");

module.exports = (gulp, gulp_args, rename) => {

	if (rename.js) {
		let renameJS = rename.js;
		for (let pathToDest in renameJS) {
			if (!renameJS.hasOwnProperty(pathToDest)) continue;
			let scripts = renameJS[pathToDest];
			for (let i = 0; i < scripts.length; i++) {
				let script = scripts[i];
				script.dest = pathToDest;
				gulp_args.build_tasks.push(`compile_rename_js_${pathToDest}_${i}`);
				gulp.task(`compile_rename_js_${pathToDest}_${i}`, compileJS(script));
			}
		}
	}
	if (rename.css) {
		let renameCSS = rename.css;
		for (let pathToDest in renameCSS) {
			if (!renameCSS.hasOwnProperty(pathToDest)) continue;
			let scripts = renameCSS[pathToDest];
			for (let i = 0; i < scripts.length; i++) {
				let script = scripts[i];
				script.dest = pathToDest;
				gulp_args.build_tasks.push(`compile_rename_css_${pathToDest}_${i}`);
				gulp.task(`compile_rename_css_${pathToDest}_${i}`, compileCSS(script));
			}
		}
	}
	
	if (rename.other) {
		let renameOther = rename.other;
		for (let pathToDest in renameOther) {
			if (!renameOther.hasOwnProperty(pathToDest)) continue;
			let scripts = renameOther[pathToDest];
			for (let i = 0; i < scripts.length; i++) {
				let script = scripts[i];
				script.dest = pathToDest;
				gulp_args.build_tasks.push(`compile_rename_other_${pathToDest}_${i}`);
				gulp.task(`compile_rename_other_${pathToDest}_${i}`, compileOther(script));
			}
		}
	}

	function compileJS(opts) {
		return () => {
			return gulp.src(opts.src, { cwd: gulp_args.cwd })
					.pipe(gRename(path => {
						// console.log('gRename', JSON.stringify(path, null, 4));
						path.basename = opts.name;
						return path;
					}))
					.pipe(gulp.dest(opts.dest, { cwd: gulp_args.cwd }));
		}
	}
	function compileCSS(opts) {
		return () => {
			return gulp.src(opts.src, { cwd: gulp_args.cwd })
					.pipe(gRename(path => {
						// console.log('gRename', JSON.stringify(path, null, 4));
						path.basename = opts.name;
						return path;
					}))
					.pipe(gulp.dest(opts.dest, { cwd: gulp_args.cwd }));
		}
	}
	function compileOther(opts) {
		return () => {
			return gulp.src(opts.src, { cwd: gulp_args.cwd })
					.pipe(gRename(path => {
						// console.log('gRename', JSON.stringify(path, null, 4));
						path.basename = opts.name;
						return path;
					}))
					.pipe(gulp.dest(opts.dest, { cwd: gulp_args.cwd }));
		}
	}
};